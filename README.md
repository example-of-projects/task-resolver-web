# TaskResolverWeb
[![Netlify Status](https://api.netlify.com/api/v1/badges/076ff2cb-16f1-4ced-a4e6-0a37948fa908/deploy-status)](https://task-resover.netlify.app/)
[![Build Status](https://travis-ci.com/example-of-projects/task-resolver-web.svg?branch=develop)](https://travis-ci.com/gitlab/example-of-projects/task-resolver-web)
[![Sonarcloud Status](https://sonarcloud.io/api/project_badges/measure?project=example-of-projects_task-resolver-web&metric=alert_status)](https://sonarcloud.io/dashboard?id=example-of-projects_task-resolver-web)
[![codecov](https://codecov.io/gl/example-of-projects/task-resolver-web/branch/develop/graph/badge.svg?token=4O0MO1G9M0)](https://codecov.io/gl/example-of-projects/task-resolver-web)

[![Technical Debt](https://sonarcloud.io/api/project_badges/measure?project=example-of-projects_task-resolver-web&metric=sqale_index)](https://sonarcloud.io/dashboard?id=example-of-projects_task-resolver-web)
[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=example-of-projects_task-resolver-web&metric=bugs)](https://sonarcloud.io/dashboard?id=example-of-projects_task-resolver-web)
[![Code Smells](https://sonarcloud.io/api/project_badges/measure?project=example-of-projects_task-resolver-web&metric=code_smells)](https://sonarcloud.io/dashboard?id=example-of-projects_task-resolver-web)
